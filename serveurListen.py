#!/usr/bin/env python

from http.server import BaseHTTPRequestHandler, HTTPServer
import json
import time
from datetime import datetime, timedelta
import logging 
#write data to table loralog:


class S(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()


    def do_POST(self):
        #execute on receiving HTTP POST
        content_length = int(self.headers['Content-Length'])
        print(content_length)
        post_data = self.rfile.read(content_length)
        json_data=json.loads(post_data)
        # lora_payload=json_data['payload_fields']
        # compteur=lora_payload['compteur']
        # temp=lora_payload['tempereture']
        # humidite=lora_payload['humidite']
        # print("Compteur :",compteur)
        # print("Temperature :", temp)
        # print("Humidite :",humidite)
        self._set_headers()

def run(server_class=HTTPServer, handler_class=S,port=5542):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    logging.info('Starting webserver and waiting for post on port: %s',port)
    httpd.serve_forever()

if _name_ == "_main_":
    from sys import argv
    logging.basicConfig(filename="lora_appserver.log",level=logging.DEBUG,format="%(asctime)s - %(levelname)s - %(message)s")
    logging.info("Starting lora_appserver.py")

    if len(argv) == 2:
        run()
    else:
        run()